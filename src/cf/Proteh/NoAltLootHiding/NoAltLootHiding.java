package cf.Proteh.NoAltLootHiding;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class NoAltLootHiding extends JavaPlugin implements Listener {

	public static java.util.List NALHPlayers;
	public static java.util.List NALHSeenPlayers;
	public double reductionRate = 0.7D;
    
	public void onEnable() 
	{
		PluginManager pm = getServer().getPluginManager();
		NALHPlayers = new java.util.ArrayList();
		NALHSeenPlayers = new java.util.ArrayList();
		pm.registerEvents(this, this); // Register all events. :D
		log("NALH enabled.");
	}
	
	public void onDisable() 
	{
		NALHPlayers.clear();
		NALHSeenPlayers.clear();
		log("NALH disabled.");
	}
	
	public static void log(String s) 
	{
		System.out.println("[NoAltLootHiding] " + s);
	}
	
	public void performActions(Inventory playerinv)
	{
		for(int j = 0; j < playerinv.getSize(); j++)
		{
			if(playerinv.getItem(j) != null && playerinv.getItem(j).getType() == Material.DIAMOND)
				if(playerinv.getItem(j).getAmount() <= 10)
					playerinv.clear(j);
				else
					playerinv.getItem(j).setAmount((int)(playerinv.getItem(j).getAmount() * reductionRate));
			if(playerinv.getItem(j) != null && playerinv.getItem(j).getType() == Material.DIAMOND_BLOCK)
				if(playerinv.getItem(j).getAmount() <= 10)
					playerinv.clear(j);
				else
					playerinv.getItem(j).setAmount((int)(playerinv.getItem(j).getAmount() * reductionRate));
			if(playerinv.getItem(j) != null && playerinv.getItem(j).getType() == Material.IRON_INGOT)
				if(playerinv.getItem(j).getAmount() <= 10)
					playerinv.clear(j);
				else
					playerinv.getItem(j).setAmount((int)(playerinv.getItem(j).getAmount() * reductionRate));
			if(playerinv.getItem(j) != null && playerinv.getItem(j).getType() == Material.IRON_BLOCK)
				if(playerinv.getItem(j).getAmount() <= 10)
					playerinv.clear(j);
				else
					playerinv.getItem(j).setAmount((int)(playerinv.getItem(j).getAmount() * reductionRate));
			if(playerinv.getItem(j) != null && playerinv.getItem(j).getType() == Material.GOLD_INGOT)
				if(playerinv.getItem(j).getAmount() <= 10)
					playerinv.clear(j);
				else
					playerinv.getItem(j).setAmount((int)(playerinv.getItem(j).getAmount() * reductionRate));
			if(playerinv.getItem(j) != null && playerinv.getItem(j).getType() == Material.GOLD_BLOCK)
				if(playerinv.getItem(j).getAmount() <= 10)
					playerinv.clear(j);
				else
					playerinv.getItem(j).setAmount((int)(playerinv.getItem(j).getAmount() * reductionRate));
			if(playerinv.getItem(j) != null && playerinv.getItem(j).getType() == Material.OBSIDIAN)
				if(playerinv.getItem(j).getAmount() <= 10)
					playerinv.clear(j);
				else
					playerinv.getItem(j).setAmount((int)(playerinv.getItem(j).getAmount() * reductionRate));
			if(playerinv.getItem(j) != null && playerinv.getItem(j).getType() == Material.TNT)
				if(playerinv.getItem(j).getAmount() <= 10)
					playerinv.clear(j);
				else
					playerinv.getItem(j).setAmount((int)(playerinv.getItem(j).getAmount() * reductionRate));
			if(playerinv.getItem(j) != null && playerinv.getItem(j).getType() == Material.GOLDEN_APPLE)
				if(playerinv.getItem(j).getAmount() <= 10)
					playerinv.clear(j);
				else
					playerinv.getItem(j).setAmount((int)(playerinv.getItem(j).getAmount() * reductionRate));
			if(playerinv.getItem(j) != null && playerinv.getItem(j).getTypeId() == 351) // Lapis
				if(playerinv.getItem(j).getAmount() <= 10)
					playerinv.clear(j);
				else
					playerinv.getItem(j).setAmount((int)(playerinv.getItem(j).getAmount() * reductionRate));
			if(playerinv.getItem(j) != null && playerinv.getItem(j).getType() == Material.LAPIS_BLOCK)
				if(playerinv.getItem(j).getAmount() <= 10)
					playerinv.clear(j);
				else
					playerinv.getItem(j).setAmount((int)(playerinv.getItem(j).getAmount() * reductionRate));
			if(playerinv.getItem(j) != null && playerinv.getItem(j).getType() == Material.LAPIS_ORE)
				if(playerinv.getItem(j).getAmount() <= 10)
					playerinv.clear(j);
				else
					playerinv.getItem(j).setAmount((int)(playerinv.getItem(j).getAmount() * reductionRate));
			if(playerinv.getItem(j) != null && playerinv.getItem(j).getType() == Material.REDSTONE_ORE)
				if(playerinv.getItem(j).getAmount() <= 10)
					playerinv.clear(j);
				else
					playerinv.getItem(j).setAmount((int)(playerinv.getItem(j).getAmount() * reductionRate));
			if(playerinv.getItem(j) != null && playerinv.getItem(j).getType() == Material.IRON_ORE)
				if(playerinv.getItem(j).getAmount() <= 10)
					playerinv.clear(j);
				else
					playerinv.getItem(j).setAmount((int)(playerinv.getItem(j).getAmount() * reductionRate));
			if(playerinv.getItem(j) != null && playerinv.getItem(j).getType() == Material.DIAMOND_ORE)
				if(playerinv.getItem(j).getAmount() <= 10)
					playerinv.clear(j);
				else
					playerinv.getItem(j).setAmount((int)(playerinv.getItem(j).getAmount() * reductionRate));
			if(playerinv.getItem(j) != null && playerinv.getItem(j).getType() == Material.GLOWSTONE)
				if(playerinv.getItem(j).getAmount() <= 10)
					playerinv.clear(j);
				else
					playerinv.getItem(j).setAmount((int)(playerinv.getItem(j).getAmount() * reductionRate));
			if(playerinv.getItem(j) != null && playerinv.getItem(j).getType() == Material.GLOWSTONE_DUST)
				if(playerinv.getItem(j).getAmount() <= 10)
					playerinv.clear(j);
				else
					playerinv.getItem(j).setAmount((int)(playerinv.getItem(j).getAmount() * reductionRate));
		}
	}
	
    @EventHandler
    public void whenPlayerJoins(PlayerJoinEvent event) 
    {
    	Player player = event.getPlayer();
    	Inventory playerinv = player.getInventory();
    	
		if(!NALHSeenPlayers.contains(player.getName().toLowerCase()))
		{
			NoAltLootHidingPlayer newnalhplayer = new NoAltLootHidingPlayer(player.getName(), 0);
			NALHSeenPlayers.add(player.getName().toLowerCase());
			NALHPlayers.add(newnalhplayer);
			return;
		}
		
    	for(int i = 0; i < NALHPlayers.size(); i++)
    	{
    		NoAltLootHidingPlayer nalhplayer = (NoAltLootHidingPlayer)NALHPlayers.get(i);
    		if(player.getName().equals(nalhplayer.getName()))
    		{
    			if(player.getPlayerTime() - nalhplayer.getLastLogoutTimestamp() >= 5200)
    			{
    				performActions(playerinv);
    				player.updateInventory();
    				player.sendMessage(ChatColor.RED + "[Notice] " + ChatColor.DARK_GRAY + "Your valuables have been reduced by 30% due to you logging off with them.");
    				return;
    			}
    		}
    	}
    }
    
    @EventHandler
    public void whenPlayerMoves(PlayerMoveEvent event) 
    {
    	Player player = event.getPlayer();
    	Inventory playerinv = player.getInventory();
    	
    	for(int i = 0; i < NALHPlayers.size(); i++)
    	{
    		NoAltLootHidingPlayer nalhplayer = (NoAltLootHidingPlayer)NALHPlayers.get(i);
    		if(nalhplayer.getName().equals(player.getName()))
    		{
    			boolean ban = false;
    			for(int j = 0; j < playerinv.getSize(); j++)
    			{
    				if(!player.hasPermission("nalh.noillegalblockban") || !player.isOp())
    				{
    					if(playerinv.getItem(j) != null && playerinv.getItem(j).getType() == Material.BEDROCK && playerinv.getItem(j).getAmount() > 0)
    						ban = true;
    					if(playerinv.getItem(j) != null && playerinv.getItem(j).getType() == Material.SPONGE && playerinv.getItem(j).getAmount() > 0)
    						ban = true;
    					if(playerinv.getItem(j) != null && playerinv.getItem(j).getType() == Material.ICE && playerinv.getItem(j).getAmount() > 0)
    						ban = true;
    					if(playerinv.getItem(j) != null && playerinv.getItem(j).getType() == Material.MOB_SPAWNER && playerinv.getItem(j).getAmount() > 0)
    						ban = true;
    					if(playerinv.getItem(j) != null && playerinv.getItem(j).getType() == Material.FIRE && playerinv.getItem(j).getAmount() > 0)
    						ban = true;
    					if(playerinv.getItem(j) != null && playerinv.getItem(j).getType() == Material.PORTAL && playerinv.getItem(j).getAmount() > 0)
    						ban = true;
    				
    					if(ban)
    					{
    						player.getInventory().clear();
    						player.updateInventory();
    						player.kickPlayer("You have been banned for having illegal blocks");
    						player.setBanned(true);
    						String ip = player.getAddress().getAddress().getHostAddress().toString();
    						getServer().banIP(ip);
    						getServer().dispatchCommand(getServer().getConsoleSender(), "say " + nalhplayer.getName() + " was banned for having illegal blocks.");
    						return;
    					}
    				}
    			}
    		}
    	}
    }
    
    @EventHandler
    public void whenPlayerQuits(PlayerQuitEvent event) 
    {
    	Player player = event.getPlayer();
    	for(int i = 0; i < NALHPlayers.size(); i++)
    	{
    		NoAltLootHidingPlayer nalhplayer = (NoAltLootHidingPlayer)NALHPlayers.get(i);
    		if(player.getName().equals(nalhplayer.getName()))
    		{
    			nalhplayer.lastLogoutTimestamp = player.getPlayerTime();
    			nalhplayer.editPlayerData();
    			return;
    		}
    	}
    }
}

class NoAltLootHidingPlayer {
	
	private String username;
	long lastLogoutTimestamp;
	
	public NoAltLootHidingPlayer(String username, long lgt)
	{
		this.username = username;
		lastLogoutTimestamp = lgt;
	}
	
	public void editPlayerData()
	{
		for(int i = 0; i < NoAltLootHiding.NALHPlayers.size(); i++)
		{
			NoAltLootHidingPlayer player = (NoAltLootHidingPlayer)NoAltLootHiding.NALHPlayers.get(i);
			if(player.getName().equals(getName()))
			{
				NoAltLootHiding.NALHPlayers.remove(player);
				NoAltLootHiding.NALHPlayers.add(this);
				return;
			}
		}
	}
	
	public String getName() 
	{
		return username;
	}
	
	public long getLastLogoutTimestamp() 
	{
		return lastLogoutTimestamp;
	}
}